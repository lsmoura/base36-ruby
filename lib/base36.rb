require "base36/version"

module Base36
  class Error < StandardError; end

  @@base36map = '0123456789abcdefghijklmnopqrstuvwxyz';

  def self.encode(n)
    response = []
    value = n
    while (value > 0)
      response.push(@@base36map[value % 36])
      value = value/36.floor
    end

    response.reverse.join('')
  end

  def self.decode(s)
    resp = 0

    s.split('').each do |c|
      resp = resp * 36 + @@base36map.index(c)
    end

    resp
  end
end
