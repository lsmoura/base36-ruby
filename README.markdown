# base36

Convert numbers to and from [Base36 format](https://en.wikipedia.org/wiki/Base36).

## installation

Add this line to your application's Gemfile:

```ruby
gem 'base36'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install base36

## license

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## author

Sergio Moura
