RSpec.describe Base36 do
  it "has a version number" do
    expect(Base36::VERSION).not_to be nil
  end

  it "encodes 1234" do
    expect(Base36.encode(1234)).to eq('ya')
  end

  it "encodes 12" do
    expect(Base36.encode(12)).to eq('c')
  end

  it "encodes 34" do
    expect(Base36.encode(34)).to eq('y')
  end

  it "encodes 69397560" do
    expect(Base36.encode(69397560)).to eq('15bfi0')
  end

  it "decodes 1234" do
    expect(Base36.decode('ya')).to eq(1234)
  end

  it "decodes 12" do
    expect(Base36.decode('c')).to eq(12)
  end

  it "decodes 34" do
    expect(Base36.decode('y')).to eq(34)
  end

  it "decodes 69397560" do
    expect(Base36.decode('15bfi0')).to eq(69397560)
  end
end
